package com.virjar.ucrack.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * @author lei.X
 * @date 2019/4/15 2:26 PM
 */
public class UcrackProvider extends ContentProvider {


    // 表名
    public static final String PUBLICDATA_TABLE_NAME = "appPropertities";


    private Context mContext;
    SQLiteDatabase db = null;
    // 设置ContentProvider的唯一标识
    public static final String AUTOHORITY = "com.virjar.ucrack.contentprovider.ucrack";

    public static final int Public_data_Code = 1;
    public static final int User_Code = 2;

    // UriMatcher类使用:在ContentProvider 中注册URI
    private static final UriMatcher mMatcher;

    static {
        mMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        // 初始化
        mMatcher.addURI(AUTOHORITY, PUBLICDATA_TABLE_NAME, Public_data_Code);
        // 若URI资源路径 = content://com.xulei.ucrackprovider/public_data ，则返回注册码Public_data_Code
    }

    @Override
    public boolean onCreate() {


        mContext = getContext();
        // 在ContentProvider创建时对数据库进行初始化
        // 运行在主线程，故不能做耗时操作,此处仅作展示
        DBHelper mDbHelper = new DBHelper(getContext());
        db = mDbHelper.getWritableDatabase();

        //构建数据库
        db.execSQL("CREATE TABLE IF NOT EXISTS " + PUBLICDATA_TABLE_NAME + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," + " app_name TEXT,propertity TEXT,switch INTEGER)");
        return true;
    }


    /**
     * 添加数据
     */

    @Override
    public Uri insert(Uri uri, ContentValues values) {

        // 根据URI匹配 URI_CODE，从而匹配ContentProvider中相应的表名
        // 该方法在最下面
        String table = getTableName(uri);

        // 向该表添加数据
        db.insert(table, null, values);

        // 当该URI的ContentProvider数据发生变化时，通知外界（即访问该ContentProvider数据的访问者）
        mContext.getContentResolver().notifyChange(uri, null);

//        // 通过ContentUris类从URL中获取ID
//        long personid = ContentUris.parseId(uri);
//        System.out.println(personid);

        return uri;
    }

    /**
     * 查询数据
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        // 根据URI匹配 URI_CODE，从而匹配ContentProvider中相应的表名
        // 该方法在最下面
        String table = getTableName(uri);
        // 查询数据
        return db.query(table, projection, selection, selectionArgs, null, null, sortOrder, null);
    }

    /**
     * 更新数据
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {

        String table = getTableName(uri);
        return db.update(table, values, selection, selectionArgs);
    }

    /**
     * 删除数据
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table = getTableName(uri);
        return db.delete(table, selection, selectionArgs);
    }

    @Override
    public String getType(Uri uri) {

        // 由于不展示,此处不作展开
        return null;
    }


    /**
     * 根据URI匹配 URI_CODE，从而匹配ContentProvider中相应的表名
     */
    private String getTableName(Uri uri) {
        String tableName = null;
        switch (mMatcher.match(uri)) {
            case Public_data_Code:
                tableName = PUBLICDATA_TABLE_NAME;
                break;
        }
        return tableName;
    }


}
